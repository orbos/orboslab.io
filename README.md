### Our goal

Our goal is to provide the best Operating System experience possible using existing and new methodologies. See the [Philosophies](philosophies/index.html).

### Who is the Orb operating system intended user?

The intended user will be beginners, intermediate and advanced users. This includes developers providing a friendly atmosphere.

### How do you intend to provide an experience meant for all users?

By having different starting points and allowing the user to choose their difficulty level. This will allow the user to learn many different aspects of Orb operating system.

Beginners can start with a prebuilt version of the operating system that does not require a lot of technical experience.

Intermediate users can start with a light system and install binary packages to tailor the system to their needs.

Advanced users can perform an entirely source based installation with full customization support.
