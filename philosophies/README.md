### Index of Orbos philosophies

* [Why did you pick Orbos as the name?](name.html)
* [Modularity](modularity.html)
* [Quality of code](code.html)
* [Documentation](docs.html)
* [Educational experience](education.html)
* [Collaboration](collaboration.html)
