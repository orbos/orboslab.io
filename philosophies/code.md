### High standards of code quality and documentation within code.

Code should be readable to more than just a developer. This allows learning how the code works through the inline documentation.

The documentation should give a good general idea of what you are doing with your code so others can easily review, learn and expand upon it.

### More in-depth documentation.

More thorough [documentation](docs.html) is to be written in the wiki for the website in github markdown language. The information on the wiki will be reviewed and added to the site if found to be applicable.

[Name](name.html) | [Modularity](modularity.html) | [Collaboration](collaboration.html) | [Index](index.html) | [Education](education.html) | [Code](code.html) | [Documentation](docs.html)
---- | ---- | ---- | ---- | ---- | ---- | ----
 | | | | | |
