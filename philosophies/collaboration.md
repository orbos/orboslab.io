### We want a place that maximizes collaboration.

The more people working on a project the more can be done at one time. A place that is friendly and collaborative for everyone is important.

### Quality and Contribution talks.

This is something of the utmost importance. Producing quality code/documentation before it is pushed, You move up the ranks through your contribution.

### Do what you love!

It is important for people to work towards what they desire. The people working on all aspects of development should enjoy what they do. What ever you enjoy working on there is a place for you.

[Name](name.html) | [Modularity](modularity.html) | [Collaboration](collaboration.html) | [Index](index.html) | [Education](education.html) | [Code](code.html) | [Documentation](docs.html)
---- | ---- | ---- | ---- | ---- | ---- | ----
| | | | | |
