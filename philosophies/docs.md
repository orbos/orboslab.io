### Documentation is of vital importance.

Documentation is more than just within the code. We must document the standards we use throughout our process. Documentation should be created while creating the code so anyone can collaborate and help with the project.

### How to help with documentation.

The wiki is used to as a staging area for new information. Anyone is able to modify wiki articles which can then produce content used in the website.

If you have officially joined the development team you can push the modified github markdown directly to the github site. If you want to use our tools to create the html for the site you may do that as well however that is optional. Generating the html from the markdown is extremely simple.

### But I am not a programmer...

If you have technical knowledge but lack programming experience guides are quite useful. Just because you did not code the material does not mean you cannot aid the project in creating better documentation!

[Name](name.html) | [Modularity](modularity.html) | [Collaboration](collaboration.html) | [Index](index.html) | [Education](education.html) | [Code](code.html) | [Documentation](docs.html)
---- | ---- | ---- | ---- | ---- | ---- | ----
| | | | | |
