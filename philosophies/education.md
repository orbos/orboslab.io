### We want to provide a great education through the usage experience.

Everyone starts out somewhere. Usually it is at the bottom with others lifting them up through education. That educational experience can be gained through documentation and collaboration.

### Documentation.

This refers to documentation both within the code and website. Code should be well documented so it is easy to read and maintain. The website should give a more thorough overview beyond what the code does, It should tie it all together to be more understandable.

### Collaboration.

Collaboration is available through many means. IRC will be a good preliminary place to discuss ideas. Github will be the best place to discuss development progression and provide in-depth help. This way many users have access to the topics.

[Name](name.html) | [Modularity](modularity.html) | [Collaboration](collaboration.html) | [Index](index.html) | [Education](education.html) | [Code](code.html) | [Documentation](docs.html)
---- | ---- | ---- | ---- | ---- | ---- | ----
| | | | | |
