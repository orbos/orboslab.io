### What is so important about modularity?

Having a modular operating system increases many aspects of security and stability. Here are some of the major features that modularity provides:

* Filesystem independent snapshots, compression and roll back system.
* Dynamic switching of core components of the system.
    * This would be difficult in a traditional system. Instead of having to remove and add large portions you can simply deactivate and activate portions that are already preconfigured.
* Nobody can permanently modify your system because you can easily roll back.
* Easy roll backs also increase stability because if something does not work you can easily switch back to a version that does work.
* Less space used on the disk for the operating system.

[Name](name.html) | [Modularity](modularity.html) | [Collaboration](collaboration.html) | [Index](index.html) | [Education](education.html) | [Code](code.html) | [Documentation](docs.html)
---- | ---- | ---- | ---- | ---- | ---- | ----
| | | | | |
