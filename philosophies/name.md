### Play on words!

Orbos is a play on words in many respects. It is a squashed version of Orb OS. Taken each letter in Orb and transferring it into words we came up with One RoBust Operating System.

### Whats special about an Orb?

An Orb is a sphere that could contain something or not. It could be considered as a form of [encapsulation and modularization](modularity.html).

A two dimensional Orb is a circle. We do our best to go full circle within our development process. Always [reiterating and improving](code.html) what we do. This includes having proper [documentation](docs.html) on all code produced for our operating system.

[Multiple pathways](collaboration.html). Any direction you go you complete the circle, We accept there are multiple ways to do the same task. With multiple ways to accomplish the same goal we can provide a binary to source experience. Respectively easy to difficult experience.

A orb has no real beginning or end. We intend to be around for a long time and create something future generations can easily maintain. Another aspect of this is a distribution with a continuity within the [learning experience](education.html).

[Name](name.html) | [Modularity](modularity.html) | [Collaboration](collaboration.html) | [Index](index.html) | [Education](education.html) | [Code](code.html) | [Documentation](docs.html)
---- | ---- | ---- | ---- | ---- | ---- | ----
| | | | | |
