### Where do I submit bugs and get support?

We use github issues to track all bug reporting and provide support.

### Is there a listing that makes things easier?

* [Orb OS website](https://gitlab.com/orbos/orbos.gitlab.io/issues)
